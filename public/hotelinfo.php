<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<? include "title.php"; ?>
<? include "metatags.php"; ?>
</head>
<body leftmargin="0" topmargin="0">
<table cellpadding="0" cellspacing="0" border="0" width="1080" align="center">
<tr>
    <td><img src="htmlimg/lineartbar1080px.jpg" width="1080" height="50"></td>
  </tr>
<tr>
    <td><? include "menu.php"; ?></td>
  </tr>
<tr>
    <td><? include "sponsors1.php"; ?></td>
  </tr>
  <tr> 
    <td valign="top"><p>&nbsp;</p>
      <h2 align="center"><font face="Arial, Helvetica, sans-serif"><em>Holiday 
        Inn @ Portland Airport, PDX<br>
        8439 NE Columbia Blvd Portland, Oregon</em></font></h2>
      <p align="center">RESERVATIONS 503-256-5000 </p>
      <p align="center">FREE 24/7 Airport Shuttle Service </p>
      <p align="center"><a href="https://www.ihg.com/holidayinn/hotels/us/en/portland/pdxap/hoteldetail?qAdlt=1&qBrs=6c.hi.ex.rs.ic.cp.in.sb.cw.cv.ul.vn.ki.sp.nd.ct&qChld=0&qFRA=1&qGRM=0&qGrpCd=CAT&qIta=99801505&qPSt=0&qRRSrt=rt&qRef=df&qRms=1&qRpn=1&qRpp=20&qSHp=1&qSmP=3&qSrt=sBR&qWch=0&srb_u=1&icdv=99801505" target="_blank"><font size=6">Click Here to Make an Online Reservation</font></a></p>
      <p align="center">Single/Double/Triple/Quad - $99.00<br>
        <br>
        Includes full hot breakfast buffet, <br>
        2 tickets per room per day</p>
      <p align="center">Single/Double/Triple/Quad - $99.00<br>
        <br>
        <font color="#FF0000"><strong>Includes full hot breakfast buffet, <br>
        2 tickets per room per day</strong></font></p>
      <p align="center">Non-refundable one-time pet fee of $25 will be charged<br>
        Rooms must be reserved by October 25, 2017</p>
      <p align="center">Mention you are with TNCC International Cat Show <br>
        <font color="#FF0000">This will also insure that you don't need to pay 
        $10 a night for parking so it is very important. </font></p>
      <blockquote> 
        <blockquote> 
          <blockquote> 
            <blockquote> 
              <blockquote>
                <p><strong>TNCC has elected to use the Hotel Room Inspection Procedures 
                  for exhibitors who lodge at the Holiday Inn. Entry in this show 
                  and lodging at Holiday Inn shall constitute an acknowledgment 
                  and consent of the exhibitor to these procedures.</strong></p>
                <p>RV PARKING (dry) available in parking lot behind Conference 
                  Center.</p>
              </blockquote>
            </blockquote>
            <p>&nbsp; </p>
          </blockquote>
        </blockquote>
      </blockquote>
      <p>&nbsp;</p></td>
  </tr>
<tr><td><? include "footer.php"; ?></td></tr>
</table>
</body>
</html>
