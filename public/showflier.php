<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<? include "title.php"; ?>
<? include "metatags.php"; ?>
</head>
<body leftmargin="0" topmargin="0">
<table cellpadding="0" cellspacing="0" border="0" width="1080" align="center">
<tr>
    <td><img src="htmlimg/lineartbar1080px.jpg" width="1080" height="50"></td>
  </tr>
<tr>
    <td><? include "menu.php"; ?></td>
  </tr>
<tr>
    <td><? include "sponsors1.php"; ?></td>
  </tr>
<tr>
    <td><div align="center">
        <p><br>
          <img src="htmlimg/ICS_Logo2017Blue_web.jpg" width="300" height="166"></p> 
        </p>
        <center>
          <font color="#0000ff" size="3" face="Arial, Helvetica, sans-serif"><strong>and 
          Food Drive for the Oregon Food Bank</strong></font> 
        </center>
        <p><font size="6"><strong>Everyone is coming to the show to have a good 
          time ...</strong></font></p>
        <p><a href="NovemberFullShowFlyer.pdf" target="_blank">Click here for 
          the Exhibitor Flier</a></p>
        <p><img src="htmlimg/banner01.jpg" width="1080" height="250"></p>
        <p>See the kitties feed the people!</p>
        <p align="center"><strong>Admission</strong> 
        <p align="center"> $8 + 2 cans of food $10 with out food <br>
          Children under 12 free 
        <p align="center"> Holiday Inn @ Portland Airport, PDX<br>
          8439 NE Columbia Blvd Portland, Oregon 
        <p align="center">TNCC PayPal Address: <a href="https://www.paypal.com/us/webapps/mpp/send-money-online" target="_blank"><font size="4">treasurer@tncc.org</font></a><br>
        </p>
        <p align="center"> 
        <table width="422" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr> 
            <td width="167" valign="top"><strong>Entry Clerk: </strong><br>
              Val Horton<br> <a href="mailto:mymains@eoni.com ">mymains@eoni.com</a><br>
              541-567-5084 <br>
              541-571-6587</td>
            <td width="18">&nbsp;</td>
            <td width="262" valign="top"> <p><strong>Show Manager/Vendors:</strong><br>
                Pamela Barrett <br>
                <a href="mailto:pam@excaliburbritish.com">pam@excaliburbritish.com</a> 
                <br>
                503-366-3490 </p></td>
          </tr>
        </table>
        <p>&nbsp;</p><table width="559" cellpadding="0" cellspacing="0" border="0" align="center">
          <tr> 
            <td width="214"><div align="center"><strong>Saturday Judges </strong></div></td>
            <td width="30">&nbsp;</td>
            <td width="46">&nbsp;</td>
            <td width="234"><div align="center"><strong>Sunday Judges </strong></div></td>
            <td width="35">&nbsp;</td>
          </tr>
          <tr> 
            <td>Tomoko Vlach</td>
            <td>AB</td>
            <td>&nbsp;</td>
            <td>Ellen Crockett </td>
            <td>AB</td>
          </tr>
          <tr> 
            <td>Heather Roberts</td>
            <td>AB</td>
            <td>&nbsp;</td>
            <td>Kurt Vlach</td>
            <td>AB</td>
          </tr>
          <tr> 
            <td>Elaine Weitz</td>
            <td>AB</td>
            <td>&nbsp;</td>
            <td>Laura Cunningham</td>
            <td>AB</td>
          </tr>
          <tr> 
            <td>Jeff Roberts</td>
            <td>AB</td>
            <td>&nbsp;</td>
            <td>Tomoko Vlach</td>
            <td>AB</td>
          </tr>
          <tr> 
            <td>Kay Hanvey</td>
            <td>AB</td>
            <td>&nbsp;</td>
            <td>Laurie Schiff</td>
            <td>AB</td>
          </tr>
          <tr> 
            <td>Kurt Vlach</td>
            <td>SP</td>
            <td>&nbsp;</td>
            <td>Alexandra Marinets</td>
            <td>SP</td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <p align="center"><strong><font color="#000000" size="3">TNCC Entry Sale; 2 days, 
          12 rings</font></strong></p>
        <p align="center"> 
        <div align="center"> 
          <table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr> 
              <td>3</td>
              <td>Entries + extra half cage</td>
              <td>$250.00</td>
            </tr>
            <tr> 
              <td>2</td>
              <td>Entries + 2 extra half cages</td>
              <td>$200.00</td>
            </tr>
            <tr> 
              <td>1</td>
              <td>Entry + extra half cage + groom space</td>
              <td>$150.00</td>
            </tr>
            <tr> 
              <td colspan="3"><div align="center">Must be same Registered Owner(s), 
                  <br>
                  Must Pay by <font color="#000000">10/15/2017</font><br>
                  Placeholders Encouraged <br>
                  Just Send Entry Info by Closing Date</div></td>
            </tr>
          </table>
        </div>
        <p align="center">Regular Entry Fees<br>
          Closing Date <font color="#000000">Oct 29, 2017</font></p>
        <div align="center"> 
          <table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr> 
              <td>2 Day Entry - Same Registered Owner</td>
              <td>1st Entry</td>
              <td><div align="right">$125</div></td>
            </tr>
            <tr> 
              <td>&nbsp;</td>
              <td>2nd Entry</td>
              <td><div align="right">$100</div></td>
            </tr>
            <tr> 
              <td>&nbsp;</td>
              <td nowrap>3rd Entry +</td>
              <td><div align="right">$75</div></td>
            </tr>
          </table>
          <p></p>
          <table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr> 
              <td width="268">1 Day Entry</td>
              <td width="78" nowrap>1st Entry +</td>
              <td width="38">$125</td>
            </tr>
          </table>
        </div>
        <p align="center">Additional Space &amp; Services</p>
        <p align="center"> 
        <table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr> 
            <td nowrap>Extra half cage, 22"X22"x22" Grooming space, 24" X 30"</td>
            <td><div align="right">$50</div></td>
          </tr>
          <tr> 
            <td>Grooming space, 24" X 30"</td>
            <td><div align="right">$50</div></td>
          </tr>
          <tr> 
            <td nowrap>Sale/Exhibition Cage, Exhibitors Only</td>
            <td><div align="right">$100</div></td>
          </tr>
          <tr> 
            <td>Additional Show Catalog</td>
            <td><div align="right">$15</div></td>
          </tr>
          <tr> 
            <td>TNCC VIP - Ring & Cake Sponsorship</td>
            <td><div align="right">$125</div></td>
          </tr>
          <tr> 
            <td>Clerking School Friday Night, RSVP # attending</td>
            <td><div align="right">Free</div></td>
          </tr>
        </table>
      <p>&nbsp;</p></div></td>
  </tr>
<tr><td><? include "footer.php"; ?></td></tr>
</table>
</body>
</html>
