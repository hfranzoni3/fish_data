<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<? include "title.php"; ?>
<? include "metatags.php"; ?>
</head>

<body leftmargin="0" topmargin="0">
<table cellpadding="0" cellspacing="0" border="0" width="1080" align="center">
  <tr> 
    <td><img src="htmlimg/lineartbar1080px.jpg" width="1080" height="50"></td>
  </tr>
  <tr> 
    <td>
      <? include "menu.php"; ?>
    </td>
  </tr>
  <tr> 
    <td>
      <? include "sponsors1.php"; ?>
    </td>
  </tr>
  <TR>
     <td valign="top"><p align="center"><img src="htmlimg/ICS_Logo2017Blue_web.jpg" width="300" height="166"></p><a href="NovemberFullShowFlyer.pdf" target="_blank"><p align="center"><font size=6><b> Download the full show flier in PDF</font></b></a></p></TD></TR>
      <p align="center"> 
      <table width="422" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="167" valign="top"><strong>Entry Clerk: </strong><br>
            Val Horton<br> <a href="mailto:mymains@eoni.com ">mymains@eoni.com</a><br>
            541-567-5084 <br>
            541-571-6587</td>
          <td width="18">&nbsp;</td>
          <td width="262" valign="top"> <p><strong>Show Manager/Vendors:</strong><br>
              Pamela Barrett <br>
              <a href="mailto:pam@excaliburbritish.com">pam@excaliburbritish.com</a> 
              <br>
              503-366-3490 </p></td>
        </tr>
      </table>
      <p>&nbsp;</p>
      <table width="559" cellpadding="0" cellspacing="0" border="0" align="center">
        <tr> 
          <td width="214"><div align="center"><strong>Saturday Judges </strong></div></td>
          <td width="30">&nbsp;</td>
          <td width="46">&nbsp;</td>
          <td width="234"><div align="center"><strong>Sunday Judges </strong></div></td>
          <td width="35">&nbsp;</td>
        </tr>
        <tr> 
          <td>Tomoko Vlach</td>
          <td>AB</td>
          <td>&nbsp;</td>
          <td>Ellen Crockett </td>
          <td>AB</td>
        </tr>
        <tr> 
          <td>Heather Roberts</td>
          <td>AB</td>
          <td>&nbsp;</td>
          <td>Kurt Vlach</td>
          <td>AB</td>
        </tr>
        <tr> 
          <td>Elaine Weitz</td>
          <td>AB</td>
          <td>&nbsp;</td>
          <td>Laura Cunningham</td>
          <td>AB</td>
        </tr>
        <tr> 
          <td>Jeff Roberts</td>
          <td>AB</td>
          <td>&nbsp;</td>
          <td>Tomoko Vlach</td>
          <td>AB</td>
        </tr>
        <tr> 
          <td>Kay Hanvey</td>
          <td>AB</td>
          <td>&nbsp;</td>
          <td>Laurie Schiff</td>
          <td>AB</td>
        </tr>
        <tr> 
          <td>Kurt Vlach</td>
          <td>SP</td>
          <td>&nbsp;</td>
          <td>Alexandra Marinets</td>
          <td>SP</td>
        </tr>
      </table>
      <blockquote> 
        <blockquote> 
          <blockquote>
            <p>&nbsp;</p>
            <p>Public Show Hours: 9 am to 5 pm Check-in time for Saturday is 7 
              am - 8:30 am, Sunday 8 am - 8:30 am. All cats must remain in the 
              show hall during advertised hours. <br>
              <br>
              People&#8217;s Choice Awards ~ Big CASH Prizes &amp; Fabulous Rosettes 
              Spectators will be voting for their favorite cat in show. TNCC will 
              award massive rosettes and give CASH to the top 3 winners each day. 
              The club will provide cage cards with entry info for display so 
              all exhibitors may participate. $100-1st ~ $ 75-2nd ~ $ 50-3rd </p>
            <p>Cage Size: Single 22&#8221; tall - 22&#8221; deep - 22&#8221; wide 
              Double 22&#8221; tall - 22&#8221; deep - 44&#8221; wide </p>
            <p> Cage curtains must cover top, sides, and bottom of benching cage. 
            </p>
            <p> TNCC provides wire-benching cages. If you need a wire cage please 
              indicate on summary sheet so we have your benching area prepared 
              just right. </p>
            <p> Services: Cat litter will be provided to all exhibitors. Please 
              bring your own food dishes and litter pans. </p>
            <p> Non-Vetted Show: All cats and kittens must be current on their 
              vaccinations. Cats with ear mites, fungus or showing signs of illness 
              will not be permitted in the show hall. Cats with suspected contagious 
              or infectious illness will be removed from the show hall. Please 
              clip claws on all four feet. </p>
            <p> Show Rules: Show Rules of The International Cat Association will 
              be in effect. You may obtain a copy of the show rules from TICA, 
              PO Box 2988, Harlingen, TX 78551. Ring awards in accordance with 
              TICA rules. </p>
            <p> Clerks: Please indicate on the entry form if you will be able 
              to clerk. Ring clerks will be paid $40 a day, or if clerking all 
              2 days receive one 2-day/1 entry TNCC Entry Special in lieu of payment. 
              All clerks will get lunch, and a fully marked electronic catalog. 
            </p>
            <p> Exhibition/For Sale Entries will only be accepted from exhibitors 
              who have cat(s) competing in this show. Please complete an official 
              entry for each cat or kitten as per TICA rules.</p>
            <p> No cat or kitten will be allowed in the Show Hall if not listed 
              in the catalog. Absolutely no sales from carriers will be permitted. 
            </p>
            <p> Household Pets: Please indicate on the entry form the Household 
              Pet&#8217;s (HHP) accurate color and pattern. If your HHP has short 
              hair but a bushy tail, it should be entered as a long hair. If your 
              HHP has a small white locket on an otherwise solid-colored body, 
              it should be entered as a solid colored cat. All HHP&#8217;s over 
              8 months of age must be ALTERED. </p>
            <p>TICA registration is not necessary to compete, but it is to claim 
              titles. Phone the entry clerk if you require assistance in completing 
              the entry form and/or the summary sheet. </p>
            <p> Preliminary New Breeds and Advanced New Breeds: According to TICA 
              Show Rule 203.19, all Preliminary New Breeds (PNB) and Advanced 
              New Breeds (ANB) must have a TICA registration number prior to any 
              TICA show. No Preliminary New Breed or Advanced New Breed may enter 
              a show with &quot;registration pending&quot;. <br>
              <br>
              Closing date Oct 29th if not earlier, as TNCC will close when 250 
              entries are reached and/or when all benching spaces are filled. 
              No Late Pages. Entry Information is Confidential. Counts will be 
              announced After Closing <br>
            </p>
          </blockquote>
        </blockquote>
      </blockquote>
      <p>&nbsp; </p></td>
  </tr>
<tr> 
    <td valign="top"><p align="center"><img src="htmlimg/ICS_Logo2017Blue_web.jpg" width="300" height="166"></p>
      <p align="center"><a href="NovemberFullShowFlyer.pdf" target="_blank">Download 
        the full show flier in PDF</a></p>
      <p align="center"><font size="4"><strong>International Cat Show Nov 2017<br>
        <font color="#FF0000">TNCC Entry Sale; 2 days, 12 rings</font></strong></font></p>
      <p align="center"> 
      <div align="center"> 
        <table width="400" border="1" align="center" cellpadding="0" cellspacing="0">
          <tr> 
            <td>3</td>
            <td>Entries + extra half cage</td>
            <td>$250.00</td>
          </tr>
          <tr> 
            <td>2</td>
            <td>Entries + 2 extra half cages</td>
            <td>$200.00</td>
          </tr>
          <tr> 
            <td>1</td>
            <td>Entry + extra half cage + groom space</td>
            <td>$150.00</td>
          </tr>
          <tr> 
            <td colspan="3"><div align="center">Must be same Registered Owner(s), 
                <br>
                Must Pay by <font color="#FF0000"><strong>10/15/2017</strong></font><br>
                Placeholders Encouraged <br>
                Just Send Entry Info by Closing Date</div></td>
          </tr>
        </table>
      </div>
      <p align="center">Regular Entry Fees<br>
        Closing Date <font color="#FF0000"><strong>Oct 29, 2017</strong></font></p>
      <div align="center"> 
        <table width="400" border="1" align="center" cellpadding="0" cellspacing="0">
          <tr> 
            <td>2 Day Entry - Same Registered Owner</td>
            <td>1st Entry</td>
            <td>$125</td>
          </tr>
          <tr> 
            <td>&nbsp;</td>
            <td>2nd Entry</td>
            <td>$100</td>
          </tr>
          <tr> 
            <td>&nbsp;</td>
            <td nowrap>3rd Entry +</td>
            <td>$75</td>
          </tr>
        </table></p>
        <table width="400" border="1" align="center" cellpadding="0" cellspacing="0">
          <tr> 
            <td width="268">1 Day Entry</td>
            <td width="78" nowrap>1st Entry +</td>
            <td width="38">$125</td>
          </tr>
        </table>
      </div>
      <p align="center">Additional Space &amp; Services</p>
      <p align="center"> 
      <table width="400" border="1" align="center" cellpadding="0" cellspacing="0">
        <tr> 
          <td nowrap>Extra half cage, 22"X22"x22" Grooming space, 24" X 30"</td>
          <td>$50</td>
        </tr>
        <tr> 
          <td>Grooming space, 24" X 30"</td>
          <td>$50</td>
        </tr>
        <tr> 
          <td nowrap>Sale/Exhibition Cage, Exhibitors Only</td>
          <td>$100</td>
        </tr>
        <tr> 
          <td>Additional Show Catalog</td>
          <td>$15</td>
        </tr>
        <tr> 
          <td>TNCC VIP - Ring & Cake Sponsorship</td>
          <td>$125</td>
        </tr>
        <tr> 
          <td>Clerking School Friday Night, RSVP # attending</td>
          <td>Free</td>
        </tr>
      </table></p>
      <p align="center">TNCC PayPal Address: <a href="https://www.paypal.com/us/webapps/mpp/send-money-online" target="_blank"><font size="4">treasurer@tncc.org</font></a><br>
      </p>
  <tr>
    <td>
      <? include "footer.php"; ?>
    </td>
  </tr>
</table>
</body>
</html>
